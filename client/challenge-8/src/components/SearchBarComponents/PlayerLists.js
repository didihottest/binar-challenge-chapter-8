import TableData from './PlayerListsComponents/TableData'
// list player component
const PlayerList = (props) => {
  return (
    <div id="list">
      <div>
        <h1 className="text-center mt-3 mb-3">Player Lists</h1>
      </div>    
      <div>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Email</th>
              <th scope="col">Username</th>
              <th scope="col">Password</th>
              <th scope="col">Experience Point</th>
              <th scope="col">Level</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {/* map search value array after filtering and map it to render all the array data */}
            {props.searchValue.map((item, index)=>{
              return <TableData item={item} index={index+1} key={index}/>
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default PlayerList;
